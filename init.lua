aliases = {}

function alias(short, cmd_table)
    aliases[short] = cmd_table
end
-- needs to be in minetest game?
-- alias("h", "home")

aliases["clc"] = { { "fp", "s1 -2 1 2" }, { "fp", "s2 2 1 -2" }, { "cl" } }
aliases["day"] = { { "time", "6000" } }
aliases["gim"] = { { "giveme" } }
aliases["grm"] = { { "grantme" } }
aliases["grma"] = { { "grantme", "all" } }
aliases["sd"] = { { "shutdown" } }
aliases["t"] = { { "teleport" } }
aliases["tr"] = { { "pulverize" } }
aliases["t0"] = { { "teleport", "0 0 0" }, { "teleport", "1 1 1" } }

-- TODO: move WorldEdit aliases here --

minetest.register_on_sending_chat_message(function(message)
	if message:sub(1,1) ~= "/" then
		return
	end

	local cmd, param = string.match(message, "^/([^ ]+) *(.*)")
	if not cmd then
        minetest.display_chat_message("-!- Empty command")
		return true
	end
    param = param or ""

    -- run each cmd pair
	if (not minetest.registered_chatcommands[cmd]) and (aliases[cmd]) then
        for i, cmd_pair in ipairs(aliases[cmd]) do
            -- only keep passed in param if first cmd and no given param
            if table.getn(cmd_pair) > 1 then
                param = cmd_pair[2]
            elseif i > 1 then
                param = ""
            end
            minetest.display_chat_message("alias for: /" .. cmd_pair[1] .. " " .. param)
            minetest.run_server_chatcommand(cmd_pair[1], param)
        end
        return true
	end
end)
